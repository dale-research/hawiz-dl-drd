from time import sleep

import cv2
import csv
import os
import numpy as np
from glob import glob
import pandas as pd
from sklearn.datasets import fetch_mldata

def load_file(file_path):
    X = []
    #y = []
    labels = {}
    file_path = os.path.abspath(file_path)
    img = cv2.imread(file_path)
    h, w, c = img.shape
    X.append(img)
    X = np.array(X).astype(np.float32)
    X = X.reshape(
        1,  # number of samples, -1 makes it so that this number is determined automatically
        3,
        h,  # first image dimension (vertical)
        w,  # second image dimension (horizontal)
    )

    return X

def load_labels(file_path):
    labels = {}
    with open("../data/diabetic_ret/trainLabels.csv", 'rb') as csvfile:
        for row in csv.reader(csvfile):
            labels[row[0]] = row[1]

    return labels

def load_data(folder_path, labels_path, verbose=0, limit=-1, size=256):
    X = []
    y = []
    labels = load_labels(labels_path)
    count = 0
    for subdir, dirs, files in os.walk(folder_path):
        for file in files:
            if verbose == 1:
                if not count % 1000:
                    print "{}. Loading image '{}'".format(count, file)
            count += 1
            path = os.path.join(subdir, file)
            if '.' not in file:
                continue

            label, file_type = file.split('.')

            img = cv2.imread(path)

            try:
                y.append(labels[label])
                X.append(img)
            except KeyError:
                continue
            if count == limit:
                break

    X = np.array(X).astype(np.float32)
    y = np.array(y).astype(np.float32)
    X = X.reshape(
        -1,  # number of samples, -1 makes it so that this number is determined automatically
        3,
        size,  # first image dimension (vertical)
        size,  # second image dimension (horizontal)
    )
    print len(X), len(y)
    return X, y

def load_mnist(path):
    X = []
    y = []
    with open(path, 'rb') as f:
        next(f)  # skip header
        for line in f:
            yi, xi = line.split(',', 1)
            y.append(yi)
            X.append(xi.split(','))

    # Theano works with fp32 precision
    X = np.array(X).astype(np.float32)
    y = np.array(y).astype(np.int32)

    # apply some very simple normalization to the data
    X -= X.mean()
    X /= X.std()

    # For convolutional layers, the default shape of data is bc01,
    # i.e. batch size x color channels x image dimension 1 x image dimension 2.
    # Therefore, we reshape the X data to -1, 1, 28, 28.
    X = X.reshape(
        -1,  # number of samples, -1 makes it so that this number is determined automatically
        1,   # 1 color channel, since images are only black and white
        28,  # first image dimension (vertical)
        28,  # second image dimension (horizontal)
    )

    return X, y