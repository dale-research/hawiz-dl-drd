import csv
import inspect
from time import sleep
import numpy
from lasagne.layers import InputLayer, Conv2DLayer
from nolearn.lasagne import NeuralNet
from pylearn2.utils import serial
import net_configs
from data import load_file, load_labels
import theano
import pylearn2
import os
from quadratic_weighted_kappa import quadratic_weighted_kappa as qwk
from utils import now, now_timestamp
import json


def test_net(net, test_dir, labels):
    # print os.path.expanduser('~/.theanorc.txt') #The below line will tell you where to put .theanoarc for configuration

    actual_values = []
    predicted_values = []
    predicted_val_dict = {}

    for subdir, dirs, files in os.walk(test_dir):
        for file in files:
            path = os.path.abspath(os.path.join(subdir, file))
            label, file_type = file.split('.')
            if file_type == 'jpeg':
                img = load_file(path)
                # prediction = net.predict(img).astype(numpy.int64)[0][0]
                prediction = net.predict(img)
                actual = labels.get(label)
                print prediction, actual
                predicted_values.append(prediction)
                actual_values.append(actual)
                predicted_val_dict[label] = prediction

    qwk_score = qwk(actual_values, predicted_values)
    return predicted_val_dict, qwk_score


if __name__ == '__main__':
    labels = load_labels("./data/diabetic_ret/trainLabels.csv");

    # net_info_list = [
    #     ('../networks/1457827404_45399_18000_NET6', 6, "../data/diabetic_ret/dataset_256_norm/test/"),
    #     ('../networks/1457038649_26229_9000_NET0', 0, "../data/diabetic_ret/dataset_256_norm/test/"),
    #     ('../networks/1457091310_52313_18000_NET0', 0, "../data/diabetic_ret/dataset_256_norm/test/"),
    #     ('../networks/1457135440_43774_18000_NET2', 2, "../data/diabetic_ret/dataset_256_norm/test/"),
    #     ('../networks/1457149414_13742_3000_NET1', 1, "../data/diabetic_ret/dataset_512_norm/test/"),
    #     ('../networks/1457163079_13434_3000_NET3', 3, "../data/diabetic_ret/dataset_512_norm/test/"),
    #     ('../networks/1457585482_38629_15000_NET4', 4, "../data/diabetic_ret/dataset_256_norm/test/"),
    #     ('../networks/1457781650_44062_18000_NET5', 5, "../data/diabetic_ret/dataset_256_norm/test/"),
    #     ('../networks/1457881574_53801_18000_NET7', 7, "../data/diabetic_ret/dataset_256_norm/test/"),
    #     ('../networks/1457921971_40038_18000_NET8', 8, "../data/diabetic_ret/dataset_256_norm/test/"),
    # ]
    net_info_list= [
        ('TESTNET', 9, "../data/diabetic_ret/dataset_256_norm/test/"),
    ]

    for net_info in net_info_list:
        if isinstance(net_info, NeuralNet):
            net = net_info
        elif isinstance(net_info, tuple):
            net_file = net_info[0]
            net_id = net_info[1]
            data_dir = net_info[2]
            input = serial.load(net_file)
            if isinstance(input, NeuralNet):
                net = input
            else:
                net = net_configs.get_net(net_id)
                net.load_params_from(input)

        print "Testing net {}....".format(net_id)

        values, qwk_score = test_net(net, data_dir, labels)



        output = {
            "net_id": net_id,
            "kappa_score": qwk_score,
            "train_history": net.train_history_,
            "predicted_values":values
        }

        print output
        serial.save("{}_NET{}".format(now(), net_id), output)


def save_conv2d_weights(net, output):
    count = 0;
    for layer in net.get_all_layers():
        count += 1;
        if isinstance(layer, Conv2DLayer):
            W_values = layer.W.get_value().tolist()
            with open(output, 'w+') as outfile:
                json.dump(W_values, outfile)
