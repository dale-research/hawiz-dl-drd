import time
import datetime

def now():
    return datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')

def now_timestamp():
    return int(time.time())